package com.yaromchikv.rxmap.presentation

import androidx.lifecycle.ViewModel
import com.google.android.gms.maps.GoogleMap
import com.yaromchikv.rxmap.common.Utils.MAIN_CITY
import com.yaromchikv.rxmap.common.Utils.MAIN_POINT
import com.yaromchikv.rxmap.domain.dto.BankObject
import com.yaromchikv.rxmap.domain.usecase.CalculateDistanceUseCase
import com.yaromchikv.rxmap.domain.usecase.GetAtmsUseCase
import com.yaromchikv.rxmap.domain.usecase.GetFilialsUseCase
import com.yaromchikv.rxmap.domain.usecase.GetInfoboxesUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import timber.log.Timber

@HiltViewModel
class MapsViewModel @Inject constructor(
    private val getFilialsUseCase: GetFilialsUseCase,
    private val getAtmsUseCase: GetAtmsUseCase,
    private val getInfoboxesUseCase: GetInfoboxesUseCase,
    private val calculateDistanceUseCase: CalculateDistanceUseCase
) : ViewModel() {

    private val _nearestObjectsState = MutableStateFlow<State>(State.Idle)
    val nearestObjectsState = _nearestObjectsState.asStateFlow()

    init {
        fetchBankObjects()
    }

    private fun fetchBankObjects() {
        Observable.zip(
            getFilialsUseCase(MAIN_CITY),
            getAtmsUseCase(MAIN_CITY),
            getInfoboxesUseCase(MAIN_CITY)
        ) { filials, atms, infoboxes -> filials + atms + infoboxes }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { _nearestObjectsState.value = State.Loading }
            .flatMapIterable { data -> data }
            .toSortedList { object1, object2 ->
                val distance1 = calculateDistanceUseCase(
                    object1.getCoordinates(),
                    MAIN_POINT.latitude to MAIN_POINT.longitude
                )
                val distance2 = calculateDistanceUseCase(
                    object2.getCoordinates(),
                    MAIN_POINT.latitude to MAIN_POINT.longitude
                )
                when {
                    distance1 < distance2 -> -1
                    distance1 > distance2 -> 1
                    else -> 0
                }
            }
            .map { bankObjects ->
                val resultList = mutableListOf<BankObject>()
                var i = 0
                while (resultList.size < 10 && i < bankObjects.size) {
                    val current = bankObjects[i]
                    if (!current.equals(resultList.lastOrNull())) {
                        resultList.add(current)
                    }
                    i++
                }
                resultList.toList()
            }
            .subscribe({ bankObjects ->
                Timber.i("onSuccess: ${bankObjects.size} BankObjects")
                bankObjects.forEach { Timber.i(it.toString()) }

                _nearestObjectsState.value = State.Ready(bankObjects)
            }, { throwable ->
                Timber.i("onError: $throwable")

                _nearestObjectsState.value = State.Error(throwable)
            })
    }

    sealed class State {
        class Ready(val data: List<BankObject>) : State()
        class Error(val error: Throwable?) : State()
        object Loading : State()
        object Idle : State()
    }
}