package com.yaromchikv.rxmap.presentation

import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.graphics.drawable.toBitmap
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.yaromchikv.rxmap.R
import com.yaromchikv.rxmap.common.Utils.MAIN_POINT
import com.yaromchikv.rxmap.databinding.ActivityMapsBinding
import com.yaromchikv.rxmap.domain.dto.AtmDto
import com.yaromchikv.rxmap.domain.dto.InfoboxDto
import dagger.hilt.android.AndroidEntryPoint
import java.net.UnknownHostException
import kotlinx.coroutines.flow.collectLatest

@AndroidEntryPoint
class MapsActivity : AppCompatActivity(R.layout.activity_maps) {

    private val binding by viewBinding(ActivityMapsBinding::bind)
    private val viewModel by viewModels<MapsViewModel>()

    private var googleMap: GoogleMap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setupMap()
    }

    private fun setupMap() {
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync {
            googleMap = it.apply {
                animateCamera(CameraUpdateFactory.newLatLngZoom(MAIN_POINT, 13f))
            }
            setupCollector()
        }
    }

    private fun setupCollector() {
        lifecycleScope.launchWhenStarted {
            viewModel.nearestObjectsState.collectLatest {
                when (it) {
                    is MapsViewModel.State.Ready -> {
                        binding.progressBar.isVisible = false
                        it.data.forEach { bankObject ->
                            addMarker(
                                position = LatLng(
                                    bankObject.getCoordinates().first,
                                    bankObject.getCoordinates().second
                                ),
                                title = when (bankObject) {
                                    is AtmDto -> getString(R.string.atm_title, bankObject.location)
                                    is InfoboxDto -> getString(R.string.infobox_title, bankObject.location)
                                    else -> bankObject.location
                                },
                                snippet = getString(
                                    R.string.address,
                                    bankObject.streetType,
                                    bankObject.street,
                                    bankObject.house
                                )
                            )
                        }
                    }
                    is MapsViewModel.State.Error -> {
                        binding.progressBar.isVisible = false
                        val message = when (it.error) {
                            is UnknownHostException -> getString(R.string.connection_error)
                            else -> it.error?.localizedMessage
                        }
                        Toast.makeText(this@MapsActivity, message, Toast.LENGTH_LONG).show()
                    }
                    is MapsViewModel.State.Loading -> {
                        binding.progressBar.isVisible = true
                    }
                    else -> Unit
                }
            }
        }
    }

    private fun addMarker(position: LatLng, title: String, snippet: String) {
        googleMap?.addMarker(
            MarkerOptions()
                .icon(getMarkerIcon())
                .position(position)
                .title(title)
                .snippet(snippet)
        )
    }

    private fun getMarkerIcon(): BitmapDescriptor {
        val bitmap = AppCompatResources.getDrawable(this, R.drawable.ic_marker)?.toBitmap()
        return if (bitmap != null) {
            BitmapDescriptorFactory.fromBitmap(bitmap)
        } else {
            BitmapDescriptorFactory.defaultMarker()
        }
    }
}