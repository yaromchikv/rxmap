package com.yaromchikv.rxmap.di

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import com.yaromchikv.rxmap.data.api.BankApi
import com.yaromchikv.rxmap.data.repository.BankRepositoryImpl
import com.yaromchikv.rxmap.domain.repository.BankRepository
import com.yaromchikv.rxmap.domain.usecase.CalculateDistanceUseCase
import com.yaromchikv.rxmap.domain.usecase.GetAtmsUseCase
import com.yaromchikv.rxmap.domain.usecase.GetFilialsUseCase
import com.yaromchikv.rxmap.domain.usecase.GetInfoboxesUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    private const val BASE_URL = "https://belarusbank.by/"

    private val moshiBuilder = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()

    @Provides
    @Singleton
    fun provideBankApi(): BankApi = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
        .addConverterFactory(MoshiConverterFactory.create(moshiBuilder))
        .build()
        .create(BankApi::class.java)

    @Provides
    @Singleton
    fun provideBankRepository(api: BankApi): BankRepository =
        BankRepositoryImpl(api)

    @Provides
    @Singleton
    fun provideGetFilialsUseCase(repository: BankRepository): GetFilialsUseCase =
        GetFilialsUseCase(repository)

    @Provides
    @Singleton
    fun provideGetAtmsUseCase(repository: BankRepository): GetAtmsUseCase =
        GetAtmsUseCase(repository)

    @Provides
    @Singleton
    fun provideGetInfoboxesUseCase(repository: BankRepository): GetInfoboxesUseCase =
        GetInfoboxesUseCase(repository)

    @Provides
    @Singleton
    fun provideCalculateDistanceUseCase() = CalculateDistanceUseCase()
}