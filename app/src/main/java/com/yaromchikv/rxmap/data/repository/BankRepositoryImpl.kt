package com.yaromchikv.rxmap.data.repository

import com.yaromchikv.rxmap.data.api.BankApi
import com.yaromchikv.rxmap.domain.dto.AtmDto
import com.yaromchikv.rxmap.domain.dto.FilialDto
import com.yaromchikv.rxmap.domain.dto.InfoboxDto
import com.yaromchikv.rxmap.domain.repository.BankRepository
import io.reactivex.rxjava3.core.Observable

class BankRepositoryImpl(
    private val apiService: BankApi
) : BankRepository {

    override fun getFilialsList(city: String): Observable<List<FilialDto>> {
        return apiService.getListOfFilials(city)
    }

    override fun getAtmsList(city: String): Observable<List<AtmDto>> {
        return apiService.getListOfATMs(city)
    }

    override fun getInfoboxesList(city: String): Observable<List<InfoboxDto>> {
        return apiService.getListOfInfoboxes(city)
    }
}