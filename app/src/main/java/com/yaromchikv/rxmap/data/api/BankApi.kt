package com.yaromchikv.rxmap.data.api

import com.yaromchikv.rxmap.domain.dto.AtmDto
import com.yaromchikv.rxmap.domain.dto.FilialDto
import com.yaromchikv.rxmap.domain.dto.InfoboxDto
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface BankApi {

    @GET("api/filials_info")
    fun getListOfFilials(
        @Query("city") city: String
    ): Observable<List<FilialDto>>

    @GET("api/atm")
    fun getListOfATMs(
        @Query("city") city: String
    ): Observable<List<AtmDto>>

    @GET("api/infobox")
    fun getListOfInfoboxes(
        @Query("city") city: String
    ): Observable<List<InfoboxDto>>
}