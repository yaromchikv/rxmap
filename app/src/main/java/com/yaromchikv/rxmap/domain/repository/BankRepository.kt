package com.yaromchikv.rxmap.domain.repository

import com.yaromchikv.rxmap.domain.dto.AtmDto
import com.yaromchikv.rxmap.domain.dto.FilialDto
import com.yaromchikv.rxmap.domain.dto.InfoboxDto
import io.reactivex.rxjava3.core.Observable

interface BankRepository {
    fun getFilialsList(city: String): Observable<List<FilialDto>>
    fun getAtmsList(city: String): Observable<List<AtmDto>>
    fun getInfoboxesList(city: String): Observable<List<InfoboxDto>>
}