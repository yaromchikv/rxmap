package com.yaromchikv.rxmap.domain.usecase

import kotlin.math.sqrt

class CalculateDistanceUseCase {
    operator fun invoke(pointA: Pair<Double, Double>, pointB: Pair<Double, Double>): Double {
        val x = pointB.first - pointA.first
        val y = pointB.second - pointA.second
        return sqrt(x * x + y * y)
    }
}