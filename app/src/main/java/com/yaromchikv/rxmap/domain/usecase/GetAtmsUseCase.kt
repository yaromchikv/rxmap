package com.yaromchikv.rxmap.domain.usecase

import com.yaromchikv.rxmap.domain.dto.AtmDto
import com.yaromchikv.rxmap.domain.repository.BankRepository
import io.reactivex.rxjava3.core.Observable

class GetAtmsUseCase(private val repository: BankRepository) {
    operator fun invoke(city: String): Observable<List<AtmDto>> {
        return repository.getAtmsList(city)
    }
}
