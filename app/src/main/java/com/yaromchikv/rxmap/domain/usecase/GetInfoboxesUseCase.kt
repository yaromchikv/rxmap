package com.yaromchikv.rxmap.domain.usecase

import com.yaromchikv.rxmap.domain.dto.InfoboxDto
import com.yaromchikv.rxmap.domain.repository.BankRepository
import io.reactivex.rxjava3.core.Observable

class GetInfoboxesUseCase(private val repository: BankRepository) {
    operator fun invoke(city: String): Observable<List<InfoboxDto>> {
        return repository.getInfoboxesList(city)
    }
}