package com.yaromchikv.rxmap.domain.usecase

import com.yaromchikv.rxmap.domain.dto.FilialDto
import com.yaromchikv.rxmap.domain.repository.BankRepository
import io.reactivex.rxjava3.core.Observable

class GetFilialsUseCase(private val repository: BankRepository) {
    operator fun invoke(city: String): Observable<List<FilialDto>> {
        return repository.getFilialsList(city)
    }
}