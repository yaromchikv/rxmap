package com.yaromchikv.rxmap.domain.dto

abstract class BankObject {
    abstract val latitude: String
    abstract val longitude: String
    abstract val location: String
    abstract val streetType: String
    abstract val street: String
    abstract val house: String

    fun getCoordinates() = latitude.toDouble() to longitude.toDouble()
}