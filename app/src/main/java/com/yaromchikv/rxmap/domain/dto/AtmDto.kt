package com.yaromchikv.rxmap.domain.dto

import com.squareup.moshi.Json

data class AtmDto(
    @Json(name = "gps_x") override val latitude: String,
    @Json(name = "gps_y") override val longitude: String,
    @Json(name = "install_place") override val location: String,
    @Json(name = "address_type") override val streetType: String,
    @Json(name = "address") override val street: String,
    @Json(name = "house") override val house: String
) : BankObject()