package com.yaromchikv.rxmap.domain.dto

import com.squareup.moshi.Json

data class FilialDto(
    @Json(name = "GPS_X") override val latitude: String,
    @Json(name = "GPS_Y") override val longitude: String,
    @Json(name = "filial_name") override val location: String,
    @Json(name = "street_type") override val streetType: String,
    @Json(name = "street") override val street: String,
    @Json(name = "home_number") override val house: String
) : BankObject()